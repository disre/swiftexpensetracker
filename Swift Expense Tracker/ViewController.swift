//
//  ViewController.swift
//  Swift Expense Tracker
//
//  Created by Kelson Vella on 10/5/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var expenses: [Expense] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "expenseCell")
        let expense = expenses[indexPath.row]
        
        if let name = expense.name {
            cell.textLabel?.text = name
            cell.detailTextLabel?.text = "$" + String(format: "%.2f", expense.amount )
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let expense = expenses[indexPath.row]
            context.delete(expense)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
        
        do {
            expenses = try context.fetch(Expense.fetchRequest())
        } catch {
            print("Error Detching")
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let expense = expenses[indexPath.row]
        performSegue(withIdentifier: "edit", sender: expense)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
        self.title = "Expenses"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
        tableView.reloadData()
    }
    
    func getData() {
        do {
            expenses = try context.fetch(Expense.fetchRequest())
        } catch {
            print("Error Fetching")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edit" {
            if let nextViewController = segue.destination as? AddViewController {
                nextViewController.expense = sender as? Expense
            }
        }
    }

}

