//
//  AddViewController.swift
//  Swift Expense Tracker
//
//  Created by Kelson Vella on 10/5/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit

class AddViewController: UIViewController, UITextFieldDelegate {

    var expense:Expense?
    
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var amountTxt: UITextField!
    @IBOutlet weak var noteTxt: UITextField!
    @IBOutlet weak var date: UIDatePicker!
    
    @IBAction func saveBtn(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        if expense != nil {
            expense?.name = nameTxt.text!
            expense?.amount = (amountTxt.text! as NSString).doubleValue
            expense?.notes = noteTxt.text
            expense?.date = date.date
        }
        else {
            let newExpense = Expense(context: context)
            newExpense.name = nameTxt.text!
            newExpense.amount = (amountTxt.text! as NSString).doubleValue
            newExpense.notes = noteTxt.text
            newExpense.date = date.date
        }
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        let _ = navigationController?.popViewController(animated: true)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameTxt.delegate = self
    self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        if expense != nil {
            self.title = "Edit"
            nameTxt.text = expense?.name
            amountTxt.text = String(format: "%.2f", (expense?.amount)!)
            noteTxt.text = expense?.notes
            date.date = (expense?.date)!
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) ->Bool {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
